<?php
namespace Westminster\Apache;

class Htaccess {
    private $rules;
    private $path;
    private $redirectFlag = false;

    public function __construct($rules, $path)
    {
        $this->rules = $rules;
        $this->path  = $path;
    }

    public function rules() { return $this->rules; }

    public function interpret($request_uri) 
    {
        $this->redirectFlag = false;
        $conditions_passed  = true;
        $apacheValues       = [
            'REQUEST_FILENAME' => '', 
            'HTTPS'            => 'off', 
            'REQUEST_URI'      => parse_url($request_uri, PHP_URL_PATH), 
            'QUERY_STRING'     => parse_url($request_uri, PHP_URL_QUERY).
                                  parse_url($request_uri, PHP_URL_FRAGMENT)
        ];
        $apacheValues['REQUEST_FILENAME'] = realpath(getcwd().$apacheValues['REQUEST_URI']);

        if ($c = strlen($this->path)) $request_uri = substr($apacheValues['REQUEST_URI'], $c);

        if (
            !($rewrite = $this->rules->search('RewriteEngine')) 
            || strtolower($rewrite->getArguments()[0]) == 'off'
        ) return $this->path.$request_uri.$apacheValues['QUERY_STRING'];

        foreach ($this->rules as $rule) {
            if (preg_match('/^RewriteCond/', $rule->getName())) {
                if (!$conditions_passed) continue;
                $args  = $rule->getArguments();
                $value = preg_replace_callback(
                    '/%{(.+)}/', 
                    function ($matches) use ($apacheValues) {
                        return $apacheValues[$matches[1]];
                    },
                    $args[0]
                );

                if (substr($args[1], 0, 1) == '!') {
                    $negate  = true;
                    $args[1] = substr($args[1], 1);
                } else {
                    $negate = false;
                }

                if ($args[1] == '-f') {
                    if (!file_exists($value)) $conditions_passed = false;
                } elseif (preg_match('#'.$args[1].'#', $value)) {
                    // regular expressions and on and off match here...
                } else {
                    $conditions_passed = false;
                }

                $conditions_passed = $negate?!$conditions_passed:$conditions_passed;

            } elseif (preg_match('/^RewriteRule/', $rule->getName())) {
                if (!$conditions_passed) { 
                    $conditions_passed = true; // reset
                    continue; // and skip this rule
                }
                $args = $rule->getArguments();
                if ($args[1] !== '-') {
                    $request_uri = preg_replace('#'.$args[0].'#', $args[1], $request_uri, 1);
                }
                if (isset($args[2])) {
                    $flags = split(',', preg_replace('/\s/', '', trim($args[2],'[]')));
                    if (in_array('R', $flags)) $this->redirectFlag = true;
                    if (in_array('L', $flags))  break;
                }
            }
        }

        if (
            preg_match('#^/#', $request_uri) # abs path
            || (($scheme = parse_url($request_uri, PHP_URL_SCHEME)) && !is_null($scheme)) # full url
        ) return $request_uri;

        return $this->path.$request_uri.$apacheValues['QUERY_STRING'];
    }

    public function redirectFlagThrown()
    {
        return $this->redirectFlag;
    }

    public static function discovery()
    {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = ltrim(preg_replace('|/[^/]+$|', '/', $uri), '/');

        while (!file_exists($uri.'.htaccess')) {
            if ($uri == '') return false;
            $uri = preg_replace('|[^/]+/$|', '', $uri);
        }

        $file   = new \SplFileObject($uri.'.htaccess');
        $parser = new \Tivie\HtaccessParser\Parser();
        $parser->ignoreWhiteLines(true)->ignoreComments(true);
        $htaccess = $parser->parse($file);
        $uri = '/'.$uri;
        return new Htaccess($htaccess, $uri);
    }
}
