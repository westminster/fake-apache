<?php
namespace Westminster\Apache;

class Router {
    const Status404 = "Document or file requested by the client was not found.";
    private $_config;
    private $_backup = [];

    public function __construct($config)
    {
        $this->_config = $config;
    }

    protected function _beforeRoute()
    {
        $this->_backup['_SERVER'] = $_SERVER;
        $this->_backup['cwd']     = getcwd();

        $_SERVER['DOCUMENT_ROOT'] = realpath($this->_config['DocumentRoot']);
        chdir($_SERVER['DOCUMENT_ROOT']);

        if ($htaccess = Htaccess::Discovery()) {
            $uri = $htaccess->interpret($_SERVER['REQUEST_URI']);
            $_SERVER['REQUEST_URI'] = $uri;
        }
    }

    protected function _afterRoute()
    {
        $_SERVER = $this->_backup['_SERVER'];
        chdir($this->_backup['cwd']);
    }

    public function runRoute() 
    {
        $this->_beforeRoute();

        $request = ltrim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');

        if (preg_match('|(^.+\.php)(/.*)$|', $request, $matches)) {
            $request              = $matches[1];
            $_SERVER['PATH_INFO'] = $matches[2];
        }

        if (empty($request) || preg_match('|/$|', $request)) {
            if (file_exists($request.'index.html')) {
                $request .= 'index.html';
            } elseif (file_exists($request.'index.php')) {
                $request .= 'index.php';
            } else {
                $this->log(404);
                $this->_afterRoute();
                return $this->_HTTPError();
            }
        } 
        if (file_exists($request)) {
            $this->log();
            $ext   = pathinfo($request, PATHINFO_EXTENSION);
            if ('php' === strtolower($ext)) {
                ob_start();
                include $request;
                $this->_afterRoute();
                return ob_get_clean();
            } else {
                $mimes = new \Mimey\MimeTypes;
            
                header("Content-type: ".$mimes->getMimeType($ext));
                $content = file_get_contents($request);
                $this->_afterRoute();
                return $content;
            }
        } else {
            $this->log(404);
            $this->_afterRoute();
            return $this->_HTTPError();
        }
    }

    protected function log($code = 200)
    {
        if (php_sapi_name() != 'cli-server') return;

        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $msg = "{$_SERVER['REMOTE_ADDR']}:{$_SERVER['REMOTE_PORT']} [$code]: $uri";
        $col = '';
        if ($code >= 500) $col = 'red';
        elseif ($code >= 400) $col = 'yellow';
        else $col = 'green';

        if (class_exists('\Colors\Color')) {
            $c   = new \Colors\Color();
            $msg = $c($msg)->fg($col);
        }
        error_log($msg);
    }

    protected function _HTTPError()
    {
        header("HTTP/1.0 404 Not Found");
        return Router::Status404;
    }
}
