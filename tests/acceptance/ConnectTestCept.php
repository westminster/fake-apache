<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('see that I hit the default site');
$I->amOnPage('/index.html');
$contents = file_get_contents(__DIR__.'/../_examples/no_htaccess/index.html');
$I->see($contents);
$I->amOnPage('/other.html');
$contents = file_get_contents(__DIR__.'/../_examples/no_htaccess/other.html');
$I->see($contents);
