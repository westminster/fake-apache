<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('see that I get the appropriate 404 header');
$I->sendGET('/does_not_exist.html');
$I->seeResponseCodeIs(404);
