<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('see that I get the correct content type for an image');
$I->sendGET('/100x100.jpg');
$I->seeHttpHeader('Content-Type', 'image/jpeg');
