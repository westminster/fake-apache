<?php
use Westminster\Apache\Htaccess;

class HtaccessTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $cwd;

    protected function _before()
    {
        $_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__.'/../_examples/simple/');
        $this->cwd = getcwd();
        chdir($_SERVER['DOCUMENT_ROOT']);
    }

    protected function _after()
    {
        unset($_SERVER['DOCUMENT_ROOT']);
        unset($_SERVER['REQUEST_URI']);
        chdir($this->cwd);
    }

    public function testFindHtaccessInRootDirectory()
    {
        $_SERVER['REQUEST_URI'] = '/index.html';
        $file                   = new \SplFileObject('.htaccess');
        $parser                 = new \Tivie\HtaccessParser\Parser();
        $parser->ignoreWhiteLines(true)->ignoreComments(true);
        $sample = $parser->parse($file);

        $output = Htaccess::discovery();

        reset($sample);
        foreach ($output as $rule) {
            list($k, $srule) = each($sample);
            $this->assertSame($srule->getName(), $rule->getName());
            $this->assertSame($srule->getArguments(), $rule->getArguments());
        }
    }

    public function testReturnFalseWhenNoHtAccess()
    {
        $_SERVER['REQUEST_URI']   = '/index.html';
        $_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__.'/../_examples/no_htaccess/');
        chdir($_SERVER['DOCUMENT_ROOT']);
        
        $this->assertFalse(Htaccess::discovery());
    }

    public function testFindHtaccessInSubDirectory()
    {
        $_SERVER['REQUEST_URI'] = '/directory/index.html';
        $file                   = new \SplFileObject('directory/.htaccess');
        $parser                 = new \Tivie\HtaccessParser\Parser();
        $parser->ignoreWhiteLines(true)->ignoreComments(true);
        $sample = $parser->parse($file);

        $output = Htaccess::discovery();

        $this->assertNotFalse($output);
        reset($sample);
        foreach ($output->rules() as $rule) {
            list($k, $srule) = each($sample);
            $this->assertSame($srule->getName(), $rule->getName());
            $this->assertSame($srule->getArguments(), $rule->getArguments());
        }
    }

    public function testFindHtaccessWhenSubDirectoryDoesNotContainIt()
    {
        $_SERVER['REQUEST_URI'] = '/no_htaccess/index.html';
        $file                   = new \SplFileObject('.htaccess');
        $parser                 = new \Tivie\HtaccessParser\Parser();
        $parser->ignoreWhiteLines(true)->ignoreComments(true);
        $sample = $parser->parse($file);

        $output = Htaccess::discovery();

        $this->assertNotFalse($output);
        reset($sample);
        foreach ($output->rules() as $rule) {
            list($k, $srule) = each($sample);
            $this->assertSame($srule->getName(), $rule->getName());
            $this->assertSame($srule->getArguments(), $rule->getArguments());
        }
    }

    public function testInvalidUrlFindHtaccessWhenSubDirectoryDoesNotContainIt()
    {
        $_SERVER['REQUEST_URI'] = '/no_htaccess/when/I/find/Nothing/index.html';
        $file                   = new \SplFileObject('.htaccess');
        $parser                 = new \Tivie\HtaccessParser\Parser();
        $parser->ignoreWhiteLines(true)->ignoreComments(true);
        $sample = $parser->parse($file);

        $output = Htaccess::discovery();

        $this->assertNotFalse($output);
        reset($sample);
        foreach ($output->rules() as $rule) {
            list($k, $srule) = each($sample);
            $this->assertSame($srule->getName(), $rule->getName());
            $this->assertSame($srule->getArguments(), $rule->getArguments());
        }
    }

    public function testBasicRewriteRule()
    {
        $htaccess = $this->_getHtaccess("RewriteEngine on\nRewriteRule ^alice.html$ bob.html\n", '/');

        $rewrite = $htaccess->interpret('/alice.html');

        $this->assertSame('/bob.html', $rewrite);
        $this->assertFalse($htaccess->redirectFlagThrown());
    }

    public function testRuleWithAbsolutePath()
    {
        $htaccess = $this->_getHtaccess("RewriteEngine on\nRewriteRule ^view.html$ /stayinformed/view.html [R,L]\n", '/penny/');

        $rewrite = $htaccess->interpret('/penny/view.html');

        $this->assertSame('/stayinformed/view.html', $rewrite);
        $this->assertTrue($htaccess->redirectFlagThrown());
    }

    public function testBasicRewriteRulePassesWithoutChange()
    {
        $htaccess = $this->_getHtaccess("RewriteEngine on\nRewriteRule ^alice.html$ -\n", '/');

        $rewrite = $htaccess->interpret('/alice.html');

        $this->assertSame('/alice.html', $rewrite);
    }

    public function testThatRewriteEngineIsSet()
    {
        $htaccess = $this->_getHtaccess("RewriteRule ^alice.html$ bob.html\n", '/');

        $rewrite = $htaccess->interpret('/alice.html');

        $this->assertSame('/alice.html', $rewrite);
    }

    public function testThatRewriteEngineIsOn()
    {
        $htaccess = $this->_getHtaccess("RewriteEngine off\nRewriteRule ^alice.html$ bob.html\n", '/');

        $rewrite = $htaccess->interpret('/alice.html');

        $this->assertSame('/alice.html', $rewrite);
    }

    public function testThatLastFlagIsObeyed()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteRule ^alice.html$ bob.html [L]\nRewriteRule ^bob.html$ alice.html",
            '/'
        );

        $rewrite = $htaccess->interpret('/alice.html');

        $this->assertSame('/bob.html', $rewrite);
    }

    public function testThatLastFlagIsObeyedEvenWhenRuleDoesNothing()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteRule ^alice.html$ - [L]\nRewriteRule ^alice.html$ bob.html",
            '/'
        );

        $rewrite = $htaccess->interpret('/alice.html');

        $this->assertSame('/alice.html', $rewrite);
    }

    public function testThatAllRulesObeyed()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteRule ^alice.html$ bob.html\nRewriteRule ^bob.html$ alice.html",
            '/'
        );

        $rewrite = $htaccess->interpret('/alice.html');

        $this->assertSame('/alice.html', $rewrite);
    }

    public function testThatSubDirectoryInfluencedCorrectly()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteRule ^alice.html$ bob.html",
            "/sub/"
        );

        $rewrite = $htaccess->interpret('/sub/alice.html');

        $this->assertSame('/sub/bob.html', $rewrite);
    }

    public function testSubstitution()
    {
        $htaccess = $this->_getHtaccess("RewriteEngine on\nRewriteRule (.*) index.php/$1", "/");

        $rewrite = $htaccess->interpret('/whatever');

        $this->assertSame('/index.php/whatever', $rewrite);
    }

    public function testRedirect()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteRule (.*) http://files1.wts.edu/$1 [R,L]", 
            "/"
        );

        $rewrite = $htaccess->interpret('/whatever');

        $this->assertSame('http://files1.wts.edu/whatever', $rewrite);
        $this->assertTrue($htaccess->redirectFlagThrown());
    }

    public function testConditionFileExists()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteCond %{REQUEST_FILENAME} -f\nRewriteRule (.*) - [L]\nRewriteRule (.*) huh/$1",
            "/"
        );

        $rewrite = $htaccess->interpret('/bob.html');

        $this->assertSame('/bob.html', $rewrite);

        $rewrite = $htaccess->interpret('/doesnotexist.html');

        $this->assertSame('/huh/doesnotexist.html', $rewrite);
    }

    public function testConditionFileDoesNotExist()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteCond %{REQUEST_FILENAME} !-f\nRewriteRule (.*) - [L]\nRewriteRule (.*) huh/$1",
            "/"
        );

        $rewrite = $htaccess->interpret('/bob.html');

        $this->assertSame('/huh/bob.html', $rewrite);

        $rewrite = $htaccess->interpret('/doesnotexist.html');

        $this->assertSame('/doesnotexist.html', $rewrite);
    }

    public function testConditionHttpsON()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteCond %{HTTPS} on\nRewriteRule (.*) - [L]\nRewriteRule (.*) huh/$1",
            "/"
        );

        $rewrite = $htaccess->interpret('/bob.html');

        $this->assertSame('/huh/bob.html', $rewrite);
    }

    public function testConditionByRegEx()
    {
        $htaccess = $this->_getHtaccess(
            "RewriteEngine on\nRewriteCond %{REQUEST_URI} ^/mobile/.*\nRewriteRule ^mobile/(.*?)/(.*) /mobilePage.php?popup_section=$1&popup_path=/$2 [R,L]\nRewriteRule (.*) huh",
            "/"
        );

        $rewrite = $htaccess->interpret('/mobile/blah/blas');

        $this->assertSame('/mobilePage.php?popup_section=blah&popup_path=/blas', $rewrite);
    }

    protected function _getHtaccess($string, $path)
    {
        $parser = (new \Tivie\HtaccessParser\Parser())->ignoreWhiteLines(true);
        $file   = new TempFile();
        $file->fwrite($string);
        $file->rewind();
        $this->assertTrue($file->isReadable());
        return new Htaccess($parser->parse($file), $path);
    }
}

class TempFile extends SPLTempFileObject {
    public function isReadable() { return true; }
}
