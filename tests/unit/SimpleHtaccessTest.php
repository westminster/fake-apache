<?php
use Westminster\Apache\Router;

class SimpleHtaccessTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $router;
    protected $path;
    protected $routerConfig = [
        'DocumentRoot' => __DIR__.'/../_examples/simple'
    ];

    protected function _before()
    {
        $this->path               = $this->routerConfig['DocumentRoot'].'/';
        $this->router             = new Router($this->routerConfig);
        $_SERVER['REMOTE_ADDR']   = '127.0.0.1';
        $_SERVER['REMOTE_PORT']   = '46464';
    }

    protected function _after()
    {
        unset($_SERVER['REQUEST_URI']);
        unset($_SERVER['REMOTE_ADDR']);
        unset($_SERVER['REMOTE_PORT']);
    }

    protected function _getFile($file)
    {
        return file_get_contents($this->path.$file);
    }
    // tests
    public function testGetIndex()
    {
        $contents               = $this->_getFile('index.html');
        $_SERVER['REQUEST_URI'] = '/index.html';

        $output = $this->router->runRoute();

        $this->assertSame(trim($contents), trim($output));
    }

    public function testGetAliceGetsBobInstead()
    {
        $contents               = $this->_getFile('bob.html');
        $_SERVER['REQUEST_URI'] = '/alice.html';

        $output = $this->router->runRoute();

        $this->assertSame(trim($contents), trim($output));
    }

    public function testSubDirectoryGetAFile()
    {
        $contents = $this->_getFile('directory/afile.html');
        $_SERVER['REQUEST_URI'] = '/directory/afile.html';

        $output = $this->router->runRoute();

        $this->assertSame(trim($contents), trim($output));
    }

    public function testSubDirectoryGetAllOthers()
    {
        $contents = $this->_getFile('directory/allothers.html');
        $_SERVER['REQUEST_URI'] = '/directory/alice.html';

        $output = $this->router->runRoute();

        $this->assertSame(trim($contents), trim($output));
    }
}
