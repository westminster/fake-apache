<?php
use Westminster\Apache\Router;

class BasicRouterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $router;
    protected $path;
    protected $routerConfig = [
        'DocumentRoot' => __DIR__.'/../_examples/no_htaccess'
    ];

    protected function _before()
    {
        $this->path               = $this->routerConfig['DocumentRoot'].'/';
        $this->router             = new Router($this->routerConfig);
        $_SERVER['REMOTE_ADDR']   = '127.0.0.1';
        $_SERVER['REMOTE_PORT']   = '46464';
    }

    protected function _after()
    {
        unset($_SERVER['REQUEST_URI']);
        unset($_SERVER['REMOTE_ADDR']);
        unset($_SERVER['REMOTE_PORT']);
    }

    protected function _getFile($file)
    {
        return file_get_contents($this->path.$file);
    }
    // tests
    public function testGetIndex()
    {
        $contents               = $this->_getFile('index.html');
        $_SERVER['REQUEST_URI'] = '/index.html';

        $output = $this->router->runRoute();

        $this->assertSame(trim($contents), trim($output));
    }

    public function testGetIndexWithSubPage()
    {
        $contents               = $this->_getFile('index/index.html');
        $_SERVER['REQUEST_URI'] = '/index/';

        $output = $this->router->runRoute();

        $this->assertSame(trim($contents), trim($output));
    }

    public function testGet404WhereNoIndex()
    {
        $_SERVER['REQUEST_URI'] = '/noindex/';

        $output = $this->router->runRoute();

        $this->assertSame(Router::Status404, trim($output));
    }

    public function testGetOtherFile()
    {
        $contents               = $this->_getFile('other.html');
        $_SERVER['REQUEST_URI'] = '/other.html';

        $output = $this->router->runRoute();

        $this->assertSame(trim($contents), trim($output));
    }

    public function testGetRootGetsIndex()
    {
        $contents               = $this->_getFile('index.html');
        $_SERVER['REQUEST_URI'] = '/';

        $output = $this->router->runRoute();

        $this->assertSame(trim($contents), trim($output));
    }

    public function test404()
    {
        $_SERVER['REQUEST_URI'] = '/does_not_exist.html';

        $output = $this->router->runRoute();

        $this->assertSame(Router::Status404, trim($output));
    }

    public function testGetPhpFile()
    {
        include($this->path."index.php");
        $_SERVER['REQUEST_URI'] = '/index.php';

        $output = $this->router->runRoute();

        $this->assertEquals($x, (int) $output);
    }

    public function testGetPhpFileWithAppendedNonsense()
    {
        include($this->path."index.php");
        $_SERVER['REQUEST_URI'] = '/index.php/index/what/what';

        $output = $this->router->runRoute();

        $this->assertEquals($x, (int) $output);
    }

    public function testDefaultToPhp()
    {
        include($this->path."phpindex/index.php");
        $_SERVER['REQUEST_URI'] = '/phpindex/';

        $output = $this->router->runRoute();

        $this->assertEquals($x, (int) $output);
    }
}
