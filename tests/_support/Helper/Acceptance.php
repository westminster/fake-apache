<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{
    private $home = __DIR__.'/../../../';
    public function _after(\Codeception\TestInterface $test) 
    {
        copy($this->home.'tests/_examples/no_htaccess.yml', $this->home.'apache.yml');
        parent::_after($test); 
    }

    public function wantToUseAlternativeConfiguration($name)
    {
        $file = $this->home.'tests/_examples/'.$name.'.yml';
        if (file_exists($file)) {
            copy($file, $this->home.'apache.yml');
        }
    }
}
